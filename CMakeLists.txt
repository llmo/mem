cmake_minimum_required(VERSION 3.0)

cmake_policy(SET CMP0048 NEW)
project(llmo_mem VERSION 0.0.0 LANGUAGES CXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 8.0)
		set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++17")
		set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++17")
	elseif (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 5.1)
		set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++1z")
		set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++1z")
	endif ()
endif ()

if (NOT DEFINED BUILD_TESTING)
	if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
		set(BUILD_TESTING TRUE)
	else ()
		set(BUILD_TESTING FALSE)
	endif ()
endif()

file(GLOB ${PROJECT_NAME}_LIST
     ${CMAKE_CURRENT_SOURCE_DIR}/*.c
     ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
     ${CMAKE_CURRENT_SOURCE_DIR}/*.cxx
     )
file(GLOB ${PROJECT_NAME}_LIST_H
     ${CMAKE_CURRENT_SOURCE_DIR}/*.h
     ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp
     ${CMAKE_CURRENT_SOURCE_DIR}/*.hxx
     )

add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_LIST} ${${PROJECT_NAME}_LIST_H})
add_library(llmo::mem ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(${PROJECT_NAME} PROPERTIES
                      CXX_STANDARD 17
                      CXX_STANDARD_REQUIRED YES
                      CXX_EXTENSIONS NO
                      )

if (NOT "${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
	target_link_libraries(${PROJECT_NAME} PRIVATE dl)
endif ()

if (BUILD_TESTING)
	enable_testing()
	add_subdirectory(tests)
endif ()
