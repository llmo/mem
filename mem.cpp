#include "mem.h"
#if __has_include( <windows.h>)
#	include <windows.h>
#else
#	define USE_POSIX_API
#	if __has_include( <sys/mman.h>) && __has_include( <unistd.h>)
#		include <sys/mman.h>
#		include <unistd.h>
#		define MMAN
#	endif
#	if __has_include( <dlfcn.h>)
#		include <dlfcn.h>
#		define DLFCN
#	endif
#endif

#ifdef USE_POSIX_API
#	include <fstream>
#	include <functional>
#	include <vector>

namespace posix_detail {
	struct memInfo {
		void *base = nullptr;
		void *end = nullptr;
		int access = 0;
		int fd = 0;
		std::string lib;
	};

	std::vector<memInfo> memCache;

	constexpr std::size_t str2int( const char *str, std::size_t len, bool hex = true ) {
		std::size_t result = 0;
		for ( std::size_t i = 0; i < len; ++i ) {
			if ( str[i] >= '0' && str[i] <= '9' ) {
				result = result * ( hex ? 0x10 : 10 ) + ( str[i] - '0' );
			} else if ( hex && str[i] >= 'a' && str[i] <= 'f' ) {
				result = result * 0x10 + 0x0A + ( str[i] - 'a' );
			} else if ( hex && str[i] >= 'A' && str[i] <= 'F' ) {
				result = result * 0x10 + 0x0A + ( str[i] - 'A' );
			}
		}
		return result;
	}
	static_assert( str2int( "1edbed7f", 8 ) == 0x1edbed7f );

	void getMemInfo( const std::function<bool( const memInfo & )> &fn, bool useCache = true ) {
		if ( useCache ) {
			for ( auto &&mi : memCache )
				if ( fn( mi ) ) return;
		}
		memCache.clear();
		std::ifstream maps( "/proc/self/maps" );
		std::string line;
		line.reserve( 0x100 );
		while ( std::getline( maps, line ) ) {
			if ( line.empty() ) continue;

			std::size_t addressLength = 0;
			while ( addressLength < line.length() && !std::isspace( line[addressLength] ) ) ++addressLength;
			if ( addressLength == line.length() ) continue;

			auto addressSeparator = line.find( '-' );
			if ( addressSeparator == std::string::npos || addressSeparator > addressLength ) continue;

			// read memory region
			memInfo mi;
			if ( addressSeparator ) mi.base = (void *)str2int( line.c_str(), addressSeparator );
			if ( addressSeparator != addressLength - 1 )
				mi.end = (void *)str2int( line.c_str() + addressSeparator + 1, addressLength - ( addressSeparator + 1 ) );

			auto accessPos = addressLength + 1;
			while ( accessPos < line.length() && std::isspace( line[accessPos] ) ) ++accessPos;
			if ( accessPos + 3 > line.length() ) continue;

			if ( line[accessPos + 0] == 'r' ) mi.access |= llmo::mem::prot::access::READ;
			if ( line[accessPos + 1] == 'w' ) mi.access |= llmo::mem::prot::access::WRITE;
			if ( line[accessPos + 2] == 'x' ) mi.access |= llmo::mem::prot::access::EXECUTE;

			// skip unused params
			auto unkHexPos = accessPos + 4;
			while ( unkHexPos < line.length() && std::isspace( line[unkHexPos] ) ) ++unkHexPos;
			auto unkHexEnd = unkHexPos;
			while ( unkHexEnd < line.length() && !std::isspace( line[unkHexEnd] ) ) ++unkHexEnd;
			auto timePos = unkHexEnd + 1;
			while ( timePos < line.length() && std::isspace( line[timePos] ) ) ++timePos;
			auto timeEnd = timePos;
			while ( timeEnd < line.length() && !std::isspace( line[timeEnd] ) ) ++timeEnd;

			auto fdPos = timeEnd + 1;
			while ( fdPos < line.length() && std::isspace( line[fdPos] ) ) ++fdPos;
			auto fdEnd = fdPos;
			while ( fdEnd < line.length() && !std::isspace( line[fdEnd] ) ) ++fdEnd;
			mi.fd = (int)str2int( line.c_str() + fdPos, fdEnd - fdPos, false );

			// read library path
			auto libPos = fdEnd + 1;
			while ( libPos < line.length() && std::isspace( line[libPos] ) ) ++libPos;
			if ( libPos != line.length() ) mi.lib = line.substr( libPos );

			// store cache
			memCache.push_back( mi );

			// check region
			if ( fn( mi ) ) break;
		}
		maps.close();
	}
} // namespace posix_detail
#endif

std::size_t llmo::mem::pageSize() {
#ifdef MMAN
	return sysconf( _SC_PAGESIZE );
#else
	SYSTEM_INFO sysInfo;
	GetSystemInfo( &sysInfo );
	return sysInfo.dwPageSize;
#endif
}

void *llmo::mem::allocate( int pageCount, void *mem ) {
	mem = (void *)( (std::uintptr_t)mem & llmo::mem::makeMask( llmo::mem::pageSize() ) ); // force align address
#ifdef MMAN
	void *result = mmap( mem,
						 pageSize() * pageCount,
						 PROT_READ | PROT_WRITE | PROT_EXEC,
						 ( mem ? MAP_FIXED : 0 ) | MAP_ANONYMOUS | MAP_PRIVATE,
						 0,
						 0 );
	if ( result == MAP_FAILED )
		result = mmap( mem, pageSize() * pageCount, PROT_READ | PROT_WRITE, ( mem ? MAP_FIXED : 0 ) | MAP_ANONYMOUS | MAP_PRIVATE, 0, 0 );
	return result;
#else
	return VirtualAlloc( mem, pageSize() * pageCount, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE );
#endif
}

void llmo::mem::deallocate( void *mem, int pageCount ) {
#ifdef MMAN
	munmap( mem, pageSize() * pageCount );
#else
	VirtualFree( mem, pageSize() * pageCount, MEM_DECOMMIT );
#endif
}

void *llmo::mem::lib::load( const char *name ) {
#ifdef DLFCN
	return dlopen( name, RTLD_NOW );
#else
	return GetModuleHandleA( name );
#endif
}

void *llmo::mem::lib::load( const std::string &name ) {
	return load( name.c_str() );
}

void llmo::mem::lib::unload( void *lib ) {
#ifdef DLFCN
	dlclose( lib );
#else
	FreeLibrary( (HMODULE)lib );
#endif
}

std::uintptr_t llmo::mem::lib::find( const char *name ) {
	return find( name, false );
}

uintptr_t llmo::mem::lib::find( const std::string &name ) {
	return find( name.c_str(), false );
}

std::uintptr_t llmo::mem::lib::find( const char *name, bool useCache ) {
#ifdef USE_POSIX_API
	void *libBase = nullptr;
	posix_detail::getMemInfo(
		[&]( const posix_detail::memInfo &mi ) {
			if ( mi.lib.rfind( name ) == std::string::npos ) return false;
// 			if ( *reinterpret_cast<std::uint32_t *>(mi.base) != 0x46'4c'45'7f ) return false;
			libBase = mi.base;
			return true;
		},
		useCache );
	return (std::uintptr_t)libBase;
#else
	return (std::uintptr_t)GetModuleHandleA( name );
#endif
}

std::uintptr_t llmo::mem::lib::find( const std::string &name, bool useCache ) {
	return find( name.c_str(), useCache );
}

std::string llmo::mem::lib::name( std::uintptr_t base ) {
#ifdef USE_POSIX_API
	std::string result;
	posix_detail::getMemInfo( [&]( const posix_detail::memInfo &mi ) {
		if ( mi.base != (void *)base ) return false;
		result = mi.lib;
		return true;
	} );
	return result;
#else
	char buf[0x400]{ 0 };
	GetModuleFileNameA( (HMODULE)base, buf, sizeof( buf ) - 1 );
	return buf;
#endif
}

void *llmo::mem::lib::symbol( const char *lib, const char *func ) {
#ifdef DLFCN
	auto h = dlopen( lib, RTLD_NOLOAD );
	if ( h ) return dlsym( h, func );
#else
	auto h = GetModuleHandleA( lib );
	if ( h ) return (void *)GetProcAddress( h, func );
#endif
	return nullptr;
}

void *llmo::mem::lib::symbol( const std::string &lib, const std::string &func ) {
	return symbol( lib.c_str(), func.c_str() );
}

int llmo::mem::prot::get( void *address, std::size_t size ) {
	return get( address, size, false );
}

int llmo::mem::prot::get( std::uintptr_t address, std::size_t size ) {
	return get( (void *)address, size, false );
}

int llmo::mem::prot::get( void *address, std::size_t size, bool useCache ) {
	int result = -1;
#ifdef USE_POSIX_API
	posix_detail::getMemInfo(
		[&]( const posix_detail::memInfo &mi ) {
			if ( address < mi.base ) return false;
			if ( (void *)( (std::uintptr_t)address + size ) <= mi.end ) {
				if ( result == -1 || mi.access < result ) result = mi.access;
				return true;
			}
			return false;
		},
		useCache );
#else
	DWORD protect, oldVP;
	VirtualProtect( address, size, PAGE_EXECUTE_READWRITE, &protect );
	VirtualProtect( address, size, protect, &oldVP );
	if ( protect & PAGE_EXECUTE_WRITECOPY )
		result = WRITE | EXECUTE;
	else if ( protect & PAGE_EXECUTE_READWRITE )
		result = READ | WRITE | EXECUTE;
	else if ( protect & PAGE_EXECUTE_READ )
		result = READ | EXECUTE;
	else if ( protect & PAGE_EXECUTE )
		result = EXECUTE;
	else if ( protect & PAGE_WRITECOPY )
		result = WRITE;
	else if ( protect & PAGE_READWRITE )
		result = READ | WRITE;
	else if ( protect & PAGE_READONLY )
		result = READ;
	else
		result = 0;
	if ( result > 0 ) {
		if ( ( result & READ ) == READ && IsBadReadPtr( address, size ) ) result = -1;
		if ( ( result & WRITE ) == WRITE && IsBadWritePtr( address, size ) ) result = -1;
		if ( ( result & EXECUTE ) == EXECUTE && IsBadCodePtr( (FARPROC)address ) ) result = -1;
	}
#endif
	return result;
}

int llmo::mem::prot::get( std::uintptr_t address, std::size_t size, bool useCache ) {
	return get( (void *)address, size, useCache );
}

bool llmo::mem::prot::set( void *address, std::size_t size, int prot ) {
#ifdef MMAN
	auto pageMask = makeMask( pageSize() );
	for ( std::size_t i = (std::uintptr_t)address & pageMask; i <= ( ( (std::uintptr_t)address + size ) & pageMask ); i += pageSize() )
		if (mprotect( (void *)i, pageSize(), prot ) != 0)
			return false;
	return true;
#else
	return set( (std::uintptr_t)address, size, prot );
#endif
}

bool llmo::mem::prot::set( std::uintptr_t address, std::size_t size, int prot ) {
#ifdef MMAN
	return set( (void *)address, size, prot );
#else
	if ( prot & ( READ | WRITE | EXECUTE ) )
		prot = PAGE_EXECUTE_READWRITE;
	else if ( prot & ( WRITE | EXECUTE ) )
		prot = PAGE_EXECUTE_WRITECOPY;
	else if ( prot & ( READ | EXECUTE ) )
		prot = PAGE_EXECUTE_READ;
	else if ( prot & EXECUTE )
		prot = PAGE_EXECUTE;
	else if ( prot & ( READ | WRITE ) )
		prot = PAGE_READWRITE;
	else if ( prot & READ )
		prot = PAGE_READONLY;
	else if ( prot & WRITE )
		prot = PAGE_WRITECOPY;
	do {
		MEMORY_BASIC_INFORMATION mbi;
		if ( !VirtualQuery( PVOID( address ), &mbi, sizeof( mbi ) ) ) return false;
		if ( size > mbi.RegionSize )
			size -= mbi.RegionSize;
		else
			size = 0;
		DWORD oldp;
		if ( !VirtualProtect( mbi.BaseAddress, mbi.RegionSize, prot, &oldp ) ) return false;
		if ( std::uintptr_t( mbi.BaseAddress ) + mbi.RegionSize < address + size )
			address = std::uintptr_t( mbi.BaseAddress ) + mbi.RegionSize;
	} while ( size );
	return true;
#endif
}

#ifdef USE_POSIX_API
int llmo::mem::getFD( void *address, bool useCache ) {
	int result = 0;
	posix_detail::getMemInfo(
		[&]( const posix_detail::memInfo &mi ) {
			if ( address != mi.base ) return false;
			result = mi.fd;
			return true;
		},
		useCache );
	return result;
}

int llmo::mem::getFD( std::uintptr_t address, bool useCache ) {
	return getFD( (void *)address, useCache );
}

std::size_t llmo::mem::getSize( void *address, bool useCache ) {
	std::size_t result = 0;
	posix_detail::getMemInfo(
		[&]( const posix_detail::memInfo &mi ) {
			if ( address != mi.base ) return false;
			result = (std::uintptr_t)mi.end - (std::uintptr_t)mi.base;
			return true;
		},
		useCache );
	return result;
}

std::size_t llmo::mem::getSize( std::uintptr_t address, bool useCache ) {
	return getSize( (void *)address, useCache );
}
#endif


#ifdef MMAN
#	undef MMAN
#endif
#ifdef DLFCN
#	undef DLFCN
#endif
#ifdef USE_POSIX_API
#	undef USE_POSIX_API
#endif
