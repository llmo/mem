#pragma once

#include <cstddef>
#include <cstdint>
#include <string>

namespace llmo::mem {
	/**
	 * @brief Create mask for value
	 * @param value Value to create mask
	 * @return Mask
	 */
	constexpr std::size_t makeMask( std::size_t value ) { return ~( value - 1 ); }

	/**
	 * @brief Get page size in bytes
	 * @return Page size
	 */
	std::size_t pageSize();

	/**
	 * @brief Allocate new page  memory
	 * @param pageCount Count of pages
	 * @param mem Target address for allocation
	 * @return Pointer to first allocated page
	 */
	void *allocate( int pageCount = 1, void *mem = nullptr );
	/**
	 * @brief Free page memory
	 * @param mem Pojnter to first page
	 * @param pageCount Count of pages
	 */
	void deallocate( void *mem, int pageCount = 1 );

	namespace lib {
		/**
		 * @brief Load library
		 * @param name Name or path to library
		 * @return Library handle
		 */
		void *load( const char *name );
		/**
		 * @brief Load library
		 * @param name Name or path to library
		 * @return Library handle
		 */
		void *load( const std::string &name );
		/**
		 * @brief Free library
		 * @param lib Library handle
		 */
		void unload( void *lib );
		/**
		 * @brief Find pointer to library
		 * @param name Library name
		 * @return Pointer to start of library (file header)
		 */
		std::uintptr_t find( const char *name );
		/**
		 * @brief Find pointer to library
		 * @param name Library name
		 * @return Pointer to start of library (file header)
		 */
		std::uintptr_t find( const std::string &name );
		/**
		 * @brief Find pointer to library
		 * @param name Library name
		 * @param useCache Use cached regions to find library (ignored on Windows platform)
		 * @return Pointer to start of library (file header)
		 */
		std::uintptr_t find( const char *name, bool useCache );
		/**
		 * @brief Find pointer to library
		 * @param name Library name
		 * @param useCache Use cached regions to find library (ignored on Windows platform)
		 * @return Pointer to start of library (file header)
		 */
		std::uintptr_t find( const std::string &name, bool useCache );
		/**
		 * @brief Find library name by pointer
		 * @param base Pointer to start of library
		 * @return Library name
		 */
		std::string name( std::uintptr_t base );
		/**
		 * @brief Load symbol from library
		 * @param lib Library name
		 * @param func Function name
		 * @return Pointer to symbol
		 */
		void *symbol( const char *lib, const char *func );
		/**
		 * @brief Load symbol from library
		 * @param lib Library name
		 * @param func Function name
		 * @return Pointer to symbol
		 */
		void *symbol( const std::string &lib, const std::string &func );
	} // namespace lib

	namespace prot {
		enum access : int
		{
			READ = 1,
			WRITE,
			EXECUTE = 4
		};
		/**
		 * @brief Get access rights to memory region
		 * @details For multiple pages it returns minimal rights
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @return Access rights
		 */
		int get( void *address, std::size_t size = 1 );
		/**
		 * @brief Get access rights to memory region
		 * @details For multiple pages it returns minimal rights
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @return Access rights
		 */
		int get( std::uintptr_t address, std::size_t size = 1 );
		/**
		 * @brief Get access rights to memory region
		 * @details For multiple pages it returns minimal rights
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @param useCache Use cached regions to find library (ignored on Windows platform)
		 * @return Access rights
		 */
		int get( void *address, std::size_t size, bool useCache );
		/**
		 * @brief Get access rights to memory region
		 * @details For multiple pages it returns minimal rights
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @param useCache Use cached regions to find library (ignored on Windows platform)
		 * @return Access rights
		 */
		int get( std::uintptr_t address, std::size_t size, bool useCache );
		/**
		 * @brief Set access rights to memory region
		 * @details When region size not equal to page size, rights settled to the entire page(s)
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @param prot Access rights
		 */
		bool set( void *address, std::size_t size = 1, int prot = READ | WRITE | EXECUTE );
		/**
		 * @brief Set access rights to memory region
		 * @details When region size not equal to page size, rights settled to the entire page(s)
		 * @param address Address of memory region
		 * @param size Size of memory region
		 * @param prot Access rights
		 */
		bool set( std::uintptr_t address, std::size_t size = 1, int prot = READ | WRITE | EXECUTE );
	} // namespace prot

// POSIX only
#if __has_include( <sys/mman.h>) && __has_include( <unistd.h>) && __has_include( <dlfcn.h>)
/**
	 * @brief Get file descriptor for allocated memory
	 * @param address address of page
	 * @param useCache Use cached regions to find file descriptor
	 * @return file descriptor
	 */
int getFD( void *address, bool useCache = false );
/**
	 * @brief Get file descriptor for allocated memory
	 * @param address address of page
	 * @param useCache Use cached regions to find file descriptor
	 * @return file descriptor
	 */
int getFD( std::uintptr_t address, bool useCache = false );
/**
	 * @brief Get Size of allocated region
	 * @param address address of region
	 * @param useCache Use cached regions to find size
	 * @return size
	 */
std::size_t getSize( void *address, bool useCache = false );
/**
	 * @brief Get Size of allocated region
	 * @param address address of region
	 * @param useCache Use cached regions to find size
	 * @return size
	 */
std::size_t getSize( std::uintptr_t address, bool useCache = false );
#endif
} // namespace llmo::mem
