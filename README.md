[![pipeline status](https://gitlab.com/llmo/mem/badges/main/pipeline.svg)](https://gitlab.com/llmo/mem/-/commits/main)

# Memory

Unified memory operations for POSIX and Windows

## Usage
1. Add project as subproject
```cmake
add_subdirectory(mem)
```
2. Link library
```cmake
target_link_libraries($(PROJECT_NAME) PRIVATE llmo_mem)
```
3. Include headers to global space
```cmake
include_directories(mem)
```
4. ???
5. PROFIT
```c++
#include <mem.h>

namespace mem = llmo::mem;

int main(){
	auto page = mem::allocate();
	// make something with page
	mem::deallocate(page);
	
	return 0;
}
```
