#include <iostream>
#include "../mem.h"

constexpr auto kWriteValue = 1;

int main() {
	std::cout << "Allocate page" << std::endl;
	auto mem = llmo::mem::allocate();
	if ( !mem ) {
		std::cerr << "Can't allocate memory" << std::endl;
		return 1;
	}

	std::cout << "Check page access rights" << std::endl;
	auto access = llmo::mem::prot::get( mem, llmo::mem::pageSize() );
	if ( access != ( llmo::mem::prot::READ | llmo::mem::prot::WRITE | llmo::mem::prot::EXECUTE ) ) {
		std::cerr << "Invalid access " << std::hex << access << " to allocated memory " << std::hex << mem << std::endl;
		return 1;
	}

	std::cout << "Write to page" << std::endl;
	*reinterpret_cast<int *>( mem ) = kWriteValue; // Segmention fault on invalid ptr

	std::cout << "Deallocate page" << std::endl;
	llmo::mem::deallocate( mem );

	std::cout << "Check access rights" << std::endl;
	access = llmo::mem::prot::get( mem, llmo::mem::pageSize() );
	if ( access == ( llmo::mem::prot::READ | llmo::mem::prot::WRITE | llmo::mem::prot::EXECUTE ) ) {
		std::cerr << "Invalid access " << std::hex << access << " to freed memory " << std::hex << mem << std::endl;
		if ( *reinterpret_cast<int *>( mem ) == kWriteValue ) // Segmention fault on invalid ptr
			std::cerr << "Page " << std::hex << mem << " not freed" << std::endl;
		return 1;
	}

	auto vsyscall = llmo::mem::lib::find( "[vsyscall]" );
	if ( !vsyscall )
		std::cerr << "[vsyscall] is not found. This is not an error for Windows platform" << std::endl;
	else
		std::cout << "[vsyscall] at " << std::hex << vsyscall << std::endl;

	std::cout << "Main test is passed" << std::endl;
	return 0;
}
